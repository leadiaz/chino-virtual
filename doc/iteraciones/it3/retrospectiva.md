# RETROSPECTIVA

# escenario

LINEA DEL TIEMPO

:)
                     
iteracion 2 --unir el proyecto a taiga.io (irradiador visual de informacion)---documentacion iteracion2----completando casos de uso----demo(basica)---modificaciones al modelo--- corrigiendo los test---Iteracion 3->
                                                                     
:(


# reflexiones
* Los irradiadores visuales nos permiten organizar mejor lo que queremos hacer
* aun necesitamos priorizar que cosas hacer antes que otras
* Investigamos sobre herramientas que nos pueden ser utiles como taiga.io para nuestro proyecto
* no es tan dificil usar taiga.io, y nos organiza mas claramente las tareas
* dejaremos siempre la ultima version del codigo que no tire errores en el repositorio

# problemas
* el codigo del modelo esta sujeto a permanentes modificaciones, dado a las distintas ideas que tenemos
* necesitamos de una computadora de refuerzo ante las dudas

# generar entendimientos

problema generalizado:

aun falta organizarnos mejor las reuniones para debatir cuestiones del proyecto


# decidir que hacer

definir los roles (aproximados) para los integrantes del grupo

# Lecciones aprendidas

* ponernos en contacto ante modificaciones en subidas al repositorio
 

# Funciono

* Las recomendaciones acerca de las herramientas que debemos utilizar, aprender cómo se usan (Taiga, Diagramas, tablas de sprint, etc).
* Prototipo de la aplicación.
* Asignar tareas definidas en la realización del proyecto.


# No funciono

* Algunos test fallaron al cambiar detalles de la imlementación. Igualmente se pudo solucionar.

