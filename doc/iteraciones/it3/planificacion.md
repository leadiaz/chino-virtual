# Documentación de cierre (PLANIFICACION y EJECUCION)

Secuencia de tiempo en los pasos que se haran desde la iteracion pasada hasta la proxima iteracion

# Diagrama de Gantt
    * NOVIEMBRE 
N°|Actividades|m|j|v|s|d|l|m|m
-|-|-|-|-|-|-|-|-|-|
||| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
1|tercera visita de entrevista|X|
2|elaboracion de la documentacion tecnica 3ra iteracion|X|
3|establecer los roles del equipo|X|X|X
4|determinacion de prioridades de los backlogs||X|X|X
5|elaborar dummys datas para ver si funciona| | | X |X | | X|
6|correccion de errores|||||||X|X
7|proxima iteracion| |||||||X|


# Ejecución

Se llevó a cabo, después de la segunda iteración, la documentación técnica del mismo:

  ** Retrospectiva: en dónde estamos parados ahora, datos reunidos en cuanto al diseño del proyecto,
                    generación de entendimientos y consenso en el grupo en algunos aspectos.
                    
  ** Irradiadores visuales de información: se decidió utiliar como herramienta para orgnizar mejor el 
                  backlog del proyecto Taiga. Todos tenemos nuestro usuario en dicha aplicación y aportamos
                  información importante en los sprint.
                  
  ** Por cada sprint, asignamos prioridades a sus respectivos componentes.
  
  ** Llevamos a cabo un prototipo de demo (inicial).
  
  ** Agregamos funcionalidad en la implementación, con sus respectivos test.
