# Retrospectiva
En esta iteracion nos repartimos los trabajos de manera tal que cada integrante del grupo pudo dedicarle mas tiempo a una tarea mas especifica.

## Complicaciones
* Al agregarle mas funcionalidad a la pagina web, entro por un momento en conflicto con la funcionalidad existente
* Al agregarle funciones a la pagina web como tarea repartida a uno de los integrantes, este integrante se sobrecargo de tareas por el codigo que en un momento dejo de funcionar

## Aspectos negativos
* No pudimos brindar mucha ayuda al compañero con el codigo en conflicto, al no contar con un branch alternativo donde subir codigo cuando no este apto para el branch Master

## Aspectos positivos
* Pudimos reunirnos para organizar que haremos cada uno hasta la proxima iteracion
* Brainstorming a traves del grupo de whatsapp, sugiriendo ideas para incorporar al proyecto y opiniones que sobre si incorporarlas o no
 
## Aspectos a mejorar
* Estableceremos mas reuniones presenciales con el equipo, donde debatiendo entre todos podemos encontrar las soluciones a los problemas que se nos presenten