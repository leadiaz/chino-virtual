
# Retrospectiva
En esta iteracion, básicamente, utilizamos la misma técnica que la iteracón pasada en cuanto a los roles que cada uno realizó.


## Aspectos positivos
* Se pudo cumplir con lo prometido antes de lo esperado.
* Aprendimos a establecer objetivos que estén a nuestro alcance.
* Consenso grupal en cuanto a las funcionalidades a agregar en esta iteración.

 
## Aspectos a mejorar
* Estableceremos mas reuniones presenciales con el equipo, donde debatiendo entre todos podemos encontrar las soluciones a los problemas que se nos presenten


## Agradecimientos
* A Gastón por ponerse la 10 y poner en funcionamiento la interfaz de nuestro proyecto, que es con lo que va a interactuar nuestro cliente.   