
# Documentación de cierre (PLANIFICACION)

Secuencia de tiempo en los pasos que se haran desde la iteracion pasada hasta la proxima iteracion

# Diagrama de Gantt
    * NOVIEMBRE 
N°|Actividades|m|j|v|s|d|l|m|m
-|-|-|-|-|-|-|-|-|-|
||| 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 |
1|cuarta visita de entrevista|X|
2|elaboracion de la documentacion tecnica 4ta iteracion|X|
3|establecer los roles del equipo|X|X|X
4|determinacion de prioridades de los backlogs||X|X|X
5|elaborar dummys datas para ver si funciona| | | X |X | | X|
6|correccion de errores|||||||X|X
7|proxima iteracion| |||||||X|


# Planificación

* Hacer andar distintas funcionalidades como:
  * Inicio de sesión de usuario
  * Agregar productos al carrito virtual
  * Comprar productos