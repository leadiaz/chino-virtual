
# Establecer escenario

* Seguir con el desarrollo de la demo agregando funcionalidades.
* Asignar una lincencia al proyecto.
* Plantear cómo encarar las distintas tareas a realizar en el transcurso de esta semana.
* Qué roles asume cada uno.


# Reunir datos

* Agregar al desarrollo distintas interfaces.
* Agregar distintos tipos de productos propios de un supermercado, con sus respectivos datos.
* Datos particulares de los usuarios para registrarse a la app.
* Licencia del proyecto.


# Generar entendimiento

* Entre varias opciones disponibles, se acordó utilizar la licencia Apache 2.0.


# Decidir que hacer

* Cambiar nombres de los campos de los productos exhibidos en la aplicación.
* Darle funcionalidades al "registrar usuario".
* Teniendo en cuenta los datos reunidos, agregar más pruductos que se puedan comprar a la aplicación con sus respectivas observaciones.


# Conclusiones

* Al haber mas tiempo entre la iteracion pasada y actual se pudo asignar mas tiempo a las tareas planificadas.
* Con los roles bien definidos, reducimos los problemas que ocacionaban cuando varios realizaban los mismos roles.