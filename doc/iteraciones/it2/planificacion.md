# Documentación de cierre (PLANIFICACION y EJECUCION)

Secuencia de tiempo en los pasos que se haran desde la iteracion pasada hasta la proxima iteracion

# Diagrama de Gantt

N°|Actividades|O|c|t|u|b|r|e|Nov
-|-|-|-|-|-|-|-|-|-|
||| 25 | 26 | 27 | 28 | 29 | 30 | 31|01|
1|segunda visita de entrevista|X|
2|elaboracion de la documentacion tecnica 2da iteracion|X|
3|incio de irradiadores visuales de info|X|X|X
4|determinacion de prioridades de los backlogs||X|X|X
5|elaborar prototipo de demo| | | X |X | | X|
6|correccion de errores|||||||X|X
7|proxima iteracion| |||||||X|


# Ejecución

Se llevó a cabo, después de la segunda iteración, la documentación técnica del mismo:

  ** Retrospectiva: en dónde estamos parados ahora, datos reunidos en cuanto al diseño del proyecto,
                    generación de entendimientos y consenso en el grupo en algunos aspectos.
                    
  ** Irradiadores visuales de información: se decidió utiliar como herramienta para orgnizar mejor el 
                  backlog del proyecto Taiga. Todos tenemos nuestro usuario en dicha aplicación y aportamos
                  información importante en los sprint.
                  
  ** Por cada sprint, asignamos prioridades a sus respectivos componentes.
  
  ** Llevamos a cabo un prototipo de demo (inicial).
  
  ** Agregamos funcionalidad en la implementación, con sus respectivos test.