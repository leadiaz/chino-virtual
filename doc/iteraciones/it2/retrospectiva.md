# RETROSPECTIVA

# escenario

LINEA DEL TIEMPO

:)
                     
iteracion 1 --integracion continua---correccion de nombre y url del repo----documentacion iteracion1----casos de uso----demo(basica)---Iteracion 2->
                                                                     
:(


# reflexiones
* El proyecto nos llevo tiempo ponernos en un contexto comun y saber por donde comenzar
* Pudimos cumplir con lo prometido, lo cual no nos demando mucho tiempo
* debemos organizarnos un poco mejor e investigar sobre las herramientas disponibles que nos pueden facilitar el trabajo
* al fin entendimos que es una demo

# generar entendimientos

problema generalizado:

la falta de irradiadores visuales de informacion nos complica a la hora de presentarle el proyecto al cliente

# decidir que hacer

* vincular nuestro proyecto con taiga.io, para poder ver los casos de uso y organizarlos con sus respectivas prioridades


# Lecciones aprendidas

* Investigar, más a fondo, distintas herramietas que nos permitan organizar de manera más optima nuestro proyecto.
* Darle más importancia a la documentación técnica que a cuestiones relacionadas a la implementación.
* Cumplir con lo pactado en cada iteración y avanzar en otras cuestiones.
 

# Funciono

* Las recomendaciones acerca de las herramientas que debemos utilizar, aprender cómo se usan (Taiga, Diagramas, tablas de sprint, etc).
* Prototipo de la aplicación.
* Asignar tareas definidas en la realización del proyecto.


# No funciono

* Algunos test fallaron al cambiar detalles de la imlementación. Igualmente se pudo solucionar.

