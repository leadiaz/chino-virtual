## Caso de uso: CU-06 pagar con la billetera virtual
##### version 1.0 (5/11/2017)

_Descripción del usuario:_

el cliente al finalizar una compra abona con dinero que posee en la billetera virtual

## Actores

Usuario registrado.

#### precondiciones

* el usuario dispone de solo una billetera virtual


#### Secuencia normal de interacciones entre los actores y el sistema

1. El usuario selecciona pagar la compra.

2. el sistema verifica si el saldo del usuario es mayor o igual de lo que debe pagar.

3. el sistema descuenta del saldo el importe a pagar que figura en el changuito virtual

4. El sistema le muestra nuevo saldo al usuario



#### postcondicion

* se efectivizo el pago de la compra


## Extensiones / Flujos secundarios

.2. el saldo es insuficiente.

   * 2.1. el sistema le mostrara el mensaje de saldo insuficiente al usuario
   * 3.2. el usuario puede elegir si quitar productos al carrito o cancelar la compra
   
   *    3.2.1 el usuario elige quitar productos del carrito
   *    3.2.2 el usuario puede volver al paso 1
   

# Dependencias de otros casos de uso

* (ref CU-01) comprar productos
* (ref CU-03) sacar productos del changuito


