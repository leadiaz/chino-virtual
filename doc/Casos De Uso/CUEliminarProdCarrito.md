## Caso de uso: CU-03 Eliminar un producto del changuito virtual
##### version 1.0 (5/11/2017)

_Descripción del usuario:_

Un usuario desea sacar un producto de su carrito virtual

## Actores

Usuario registrado.

#### precondiciones

* el usuario esta registrado en el sistema
* el changuito del usuario tiene al menos un producto


#### Secuencia normal de interacciones entre los actores y el sistema

1. El usuario selecciona un su changuito virtual.

2. El sistema le muestra un listado de productos con sus precios individuales y cantidad deseada.

3. El usuario selecciona el producto a elminimar

4. El usuario selecciona eliminar

5. El sistema muestra un mensaje " producto eliminado exitosamente"

6. El sistema consulta al usuario si desea eliminar otro producto

  * 
        6.1. El usuario confirma y se realiza nuevamente el paso 2.



#### postcondicion

* en el listado del changuito no figura mas el producto que ha sido eliminado

## Extensiones / Flujos secundarios

.4. El usuario elimino un producto equivocado o quiere volver a añadirlo.

   * 4.1. El usuario debe volver a la compra seleccionarlo nuevamente
   

# Dependencias de otros casos de uso

* (Referencia CU-01) Comprar un Producto
