## Caso de uso: CU-02 Añadir un producto al changito virtual

##### version 1.0 (5/11/2017)

_Descripción del usuario:_

Un usuario del sistema con una sesión abierta desea realizar seleciona productos que desea comprar.

## Actores

Usuario registrado.

#### precondiciones

* el usuario esta registrado en el sistema
* el changuito del usuario se encuentra vacio al ingresar al sistema (no hay compras pendientes)


#### Secuencia normal de interacciones entre los actores y el sistema

1. El usuario selecciona un producto durante la compra.

2. El usuario selecciona agregar ese producto al carrito virtual.

3. El carrito virtual suma un producto mas a su listado

4. El sistema muestra un mensaje " producto añadido exitosamente"

5. El sistema consulta al usuario si desea añadir otro producto

  * 
        5.1. El usuario confirma y se realiza nuevamente el paso 1.



#### postcondicion

* el carrito virtual poseera un listado en donde estaran el/los productos seleccionados, su precio por unidad, cantidad pedida y precio a pagar

## Extensiones / Flujos secundarios

.1. El usuario añadio un producto equivocado o se arrepiente de añadirlo.

   * 10.1. El usuario selecciona su changito virtual y ejecutara el caso de uso eliminar producto
   

# Dependencias de otros casos de uso

* (Referencia CU-01) Comprar un Producto

* (referencia 0CV01) eliminar productos del changito virtual