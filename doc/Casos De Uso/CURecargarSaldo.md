## Caso de uso: CU-07 recargar la billetera virtual
##### version 1.0 (5/11/2017)

_Descripción del usuario:_

el empleado le carga un determinado importe a la billetera virtual de un usuario

## Actores

empleado

#### precondiciones

* el usuario abono al empleado lo que desea recargar
* los montos a recargar son numeros mayores a 0


#### Secuencia normal de interacciones entre los actores y el sistema

1. El empleado selecciona recargar saldo.

2. el sistema muestra el listado de usuarios del sistema.

3. el empleado elige al usuario que desear recargar credito

4. El sistema consulta importe a recargar

5. el empleado indica importe

6. el sistema muestra el mensaje "recarga exitosa"


#### postcondicion

* se efectivizo la carga de credito


## Extensiones / Flujos secundarios

.

# Dependencias de otros casos de uso

*

