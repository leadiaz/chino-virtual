## Caso de uso: CU-04 Comentar sobre algun un producto
##### version 1.0 (5/11/2017)

_Descripción del usuario:_

Un usuario desea realizar un comentario sobre algun producto

## Actores

Usuario registrado.

#### precondiciones

* el usuario esta registrado en el sistema
* el producto que desean Comentar se encuentra en la pagina web
* el comentario es individual por producto


#### Secuencia normal de interacciones entre los actores y el sistema

1. El usuario selecciona un un producto.

2. El usuario selecciona Comentar.

3. El sistema le abre un cuadro de dialogo para que el usuario escriba el comentario

4. El usuario escribe el comentario en el cuadro de dialogo

5. El usuario clickea en el boton publicar comentario

6. El sistema muestra un mensaje " comentario publicado exitosamente"

6. El sistema muestra en el indicador de comentarios un comentario mas



#### postcondicion

* el comentario queda en la pagina y puede ser visto por cualquier usuario sea registrado o no

## Extensiones / Flujos secundarios

.4. El usuario se arrepiente y quiere eliminar el comentario.

   * 4.1. si el usuario no pulso el boton publicar el comentario no sera publicado
   * 4.2. un comentario publicado no puede ser borrado
   

# Dependencias de otros casos de uso

* 
