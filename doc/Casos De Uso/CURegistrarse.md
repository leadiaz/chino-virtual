## Caso de uso: CU-05 Registrarse
##### version 1.0 (5/11/2017)

_Descripción del usuario:_

Una persona desea Registrarse en el sistema

## Actores

Usuario de la plataforma.

Empleado del negocio.

#### precondiciones

* la persona no debe poseer una cuenta previa en el sistema


#### Secuencia normal de interacciones entre los actores y el sistema

1. La persona selecciona la opcion de Registrarse.

2. El sistema le pide completar los datos personales.

3. la persona completa el formulario y selecciona siguiente

4. El sistema le muestra terminos y condiciones del servicio

5. la persona elige aceptar

6. El sistema le pide que indique que nombre de usuario y contraseña usara

7. El sistema valida la disponibilidad del nombre de usuario

8. el sistema muestra el mensaje "se ha registrado exitosamente"


#### postcondicion

* el quedo registrado un nuevo usuario en el sistema
* el departamento de sistemas le manda un link especial al Empleado para registrarse pero los pasos son los mismos


## Extensiones / Flujos secundarios

.3. la persona ya posee una cuenta en el sistema.

   * 3.1. la persona debera ir a iniciar sesion
   * 3.2. la persona debera seleccionar olvido su contraseña
   * 3.3. el sistema le mandara el nombre de usuario y contraseña al email
   * 3.4. el sistema mostrara un mensaje "se envio su usuario y contraseña a su correo electronico"
   
.7. el nombre de usuario esta asignado a otra persona
   * 7.1. el sistema muestra el mensaje "nombre de usuario no disponible"
   * 7.2. se vuelve al paso 6
   

# Dependencias de otros casos de uso

* 
