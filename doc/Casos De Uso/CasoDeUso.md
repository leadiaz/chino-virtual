## Caso de uso: CU-01 Comprar un producto

##### version 1.3 (29/10/2017)

_Descripción del usuario:_

Un usuario del sistema con una sesión abierta desea realizar una compra de un determinado producto.

## Actores

Usuario registrado.

#### precondiciones

* el usuario esta registrado en el sistema
* el changuito del usuario se encuentra vacio al ingresar al sistema (no hay compras pendientes)
* los productos que se muestran en la pagina, tienen al menos una unidad disponible en stock


#### Secuencia normal de interacciones entre los actores y el sistema

1. El usuario selecciona un producto.

2. El sistema agrega ese producto al carrito virtual.

3. El sistema muestra mensaje "producto añadido a su carrito virtual" 

4. El sistema consulta "¿desea añadir un producto mas a su carrito?"
    * 
        4.1. El usuario confirma y se realiza nuevamente el paso 1.

5. El usuario selecciona el changuito virtual

6. El sistema le muestra el listado de productos que añadio durante la compra, junto a la cantidad deseada, el precio por unidad y el total a pagar

7. El usuario selecciona la opcion confirmar la compra.

8. El sistema debe verificar el stock disponible del producto

9. El sistema descuenta del stock del producto una unidad (la del producto correspondiente).

10. El sistema descuenta el precio del producto del saldo del cliente.

11. El sistema muestra el mensaje "compra realizada exitosamente" 

#### postcondicion

* el sistema le da un numero de comprobante de compra al usuario
* el usuario con ese numero de comprobante puede retirar el pedido en la tienda o pedir que lo traigan por delivery


## Extensiones / Flujos secundarios

.10. El usuario no tiene saldo disponible para realizar la compra.

   * 10.1. El sistema le muestra un mensaje informando del saldo insuficiente al usuario
   
   * 10.2  El sistema le ofrece las opciones "cancelar compra", "revisar carrito"
   
       * 10.2.1. El usuario elige cancelar la compra.
           * 10.2.1.1. El sistema vacia el changuito virtual del usuario.
           * 10.2.1.2. El sistema suma nuevamente esos productos al stock.
           * 10.2.1.3. El sistema muestra el mensaje "su compra ha sido cancelada"
       
       * 10.2.2. El usuario elige revisar el changuito virtual
           * 10.2.2.1. se vuelve al paso 6


# Dependencias de otros casos de uso

* (Referencia OBS01) Control de Stock de Productos

* (referencia 0CV01) Gestion de productos en el changito virtual