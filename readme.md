***readme.md***

Universidad Nacional de Quilmes - Ingenieria de Software 2017

# Proyecto

![Chino](/doc/DocumentacionTecnica/Logo.png)
#### virtual

## Integracion continua
[![Estado del Pipeline](https://gitlab.com/unq-tpi-eis-alumnos-2017-s2/grupal--ChinoVirtual-4/badges/master/pipeline.svg)](https://gitlab.com/unq-tpi-eis-alumnos-2017-s2/grupal--ChinoVirtual-4/commits/master)

## Backlog
https://tree.taiga.io/project/alex2704-chinovirtual/

# Grupo 4
Integrantes        | Usuario                               |
-------------------|---------------------------------------|
Da Silva Gaston    | https://gitlab.com/GastonOscarDasilva |
Diaz Leandro       |https://gitlab.com/leaa                |
Quiñonez Alexander | https://gitlab.com/Alex2704           |
Rodriguez Alan     | https://gitlab.com/alannrod           |

## Descripcion:
Nuestro cliente:  Un magnate chino que posee un supermercado, desea tener una aplicación desde donde pueda publicar los productos a vender y los clientes puedan comprar a distancia para luego decidir si retiran en la tienda o se enviara el pedido a domicilio.
los clientes que quieran comprar en la plataforma deben registrarse, para ello se les piden los datos personales, se le asigna un saldo inicialmente en 0, el saldo puede recargarse cuantas veces quiera y no tiene un tope maximo. en cambio si el cliente se queda sin saldo, se le otorga un prestamo de hasta $ 200, el cual será descontado en la proxima recarga.
El carrito virtual tiene dos funcionalidades: ir almacenando la lista de productos que desea comprar el cliente e indicar cuanto es el monto total a pagar. Los productos se ingresan en el orden que el cliente los fue seleccionando pero en cambio se puede sacar cualquier producto (porque el cliente se arrepiente o decide no comprarlo) sin importar su orden.
Por el momento solo tenemos clientes prepagos, los cuales acreeditan una cierta suma de dinero en su cuenta y ese saldo se debita el importe de la compra al confirmar la misma. El saldo no tiene fecha de vencimiento y todas las acreditaciones o debitaciones de dinero se realiza en moneda local. Se prevee para el futuro incorporar otros medios de pago como tarjeta de credito, debito o pago electronico como pago facil o pim o todopago…
## Licencia
Copyright 2017 ChinoProject