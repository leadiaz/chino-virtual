package APIRest

import org.uqbar.xtrest.api.annotation.Controller
import org.uqbar.xtrest.api.annotation.Get
import org.uqbar.xtrest.http.ContentType
import org.uqbar.xtrest.json.JSONUtils
import appChino.Producto
import org.uqbar.xtrest.api.annotation.Delete
import org.uqbar.xtrest.api.annotation.Body
import org.uqbar.xtrest.api.annotation.Post
import java.util.List
import appChino.App
import org.uqbar.commons.model.UserException
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import appChino.Usuario
import appChino.Cliente
import appChino.Empleado
import rest.AppRest
import rest.ClienteRest
import org.uqbar.xtrest.api.Result
import org.uqbar.xtrest.api.annotation.Put
import appChino.CarritoVirtual

@Controller
class ChinoVirtualAPIRest {
	
	extension JSONUtils = new JSONUtils
	
	App chino

	new(App chino){
		this.chino = chino
	}
	
	@Get("/Productos")
	def getProducto(){
		response.contentType = ContentType.APPLICATION_JSON
		ok(this.chino.productos.toJson)
	}
	
	@Delete("/Productos/:id")
	def eliminarProducto(){
		response.contentType = ContentType.APPLICATION_JSON
		try{
			this.chino.eliminarProducto(Integer.valueOf(id))
			ok()
		}
		catch(NumberFormatException e){
			badRequest(getErrorJson("el id debe ser un numero entero"))
		}
		
	}
	
	@Put("/Producto/:id")
	def modificarProducto(@Body String body){
		response.contentType = ContentType.APPLICATION_JSON
        try {
	        val Producto p = body.fromJson(Producto)
	        if (Integer.parseInt(id) != p.id) {
			 badRequest('{ "error" : "Id en URL distinto del cuerpo" }');
			 }
		
	        try {
				this.chino.actualizarProducto(p)
				ok()	        	
	        } 
	        catch (UserException exception) {
	        	badRequest(getErrorJson(exception.message))
	        }
        
        }
        catch (UnrecognizedPropertyException exception) {
        	badRequest(getErrorJson("El body debe ser un producto"))
        	}

	}
	
	
	
//	@Post("/Productos")
//	def crearProducto(@Body String body){
//		response.contentType = ContentType.APPLICATION_JSON
//		try{
//			val Producto p = body.fromJson(Producto)
//			try{
//				this.chino.agregarProducto(p)
//				ok()
//			}
//			catch(UserException e) {
//				badRequest(getErrorJson(e.message))
//				
//			}
//		}
//		catch (UnrecognizedPropertyException exception) {
//			badRequest(getErrorJson("El body debe ser un Villano"))
//		}
//	}
	
/************************************
 * 				Usuarios
 ************************************/	
	
//	@Get("/Users")
//	def getUsuario(String string){
//		response.contentType = ContentType.APPLICATION_JSON
//		var res = this.chino.usuarios.filter[it.id.toString.contains(string)]
//	}
//	

	@Get("/Clientes")
	def Result getUsuario(){
		response.contentType = ContentType.APPLICATION_JSON
		var appr = new  AppRest(this.chino);
		ok(appr.clientes.toJson)
	}
	@Get("/Clientes/:id")
    def getClienteById() {
        response.contentType = ContentType.APPLICATION_JSON
        try {        	
            var cliente = this.chino.getCliente(Integer.valueOf(id))
            if (cliente == null) {
            	notFound(getErrorJson("No existe Cliente con ese id"))
            } else {
            	var ClienteRest = new ClienteRest(cliente);
            	ok(ClienteRest.toJson)
            }
        }
        catch (NumberFormatException ex) {
        	badRequest(getErrorJson("El id debe ser un numero entero"))
        }
    }
	@Get("/ClientesBuscar")
	def Result getUsuarioCliente(String nombreUsuario ){
		try {
		val nombre = String.valueOf(nombreUsuario);
		response.contentType = ContentType.APPLICATION_JSON
		var appr = new  AppRest(this.chino);
		ok(appr.buscarCliente(nombre).toJson)
		
		} catch (UserException e) {
			return notFound("No existe Usuario con ese nombre '" + nombreUsuario + "'");
		}
	}
	@Put("/Clientes/:id")
    def Result actualizarCliente(@Body String body) {
        response.contentType = ContentType.APPLICATION_JSON
	        val ClienteRest c = body.fromJson(ClienteRest)
	        var Cliente cfin = new Cliente (c,this.chino);
	        
	        if (Integer.parseInt(id) != c.id) {
			 badRequest('{ "error" : "Id en URL distinto del cuerpo" }');
			 }
			
	        try {
				this.chino.actualizarCliente(cfin)
				ok()	        	
	        } 
	        catch (UserException exception) {
	        	badRequest(getErrorJson(exception.message))
	        }
        
        }

	@Post("/ClientesAgregar")
	def Result createClient(@Body String body){
		//response.contentType = ContentType.APPLICATION_JSON
				try {
					val ClienteRest c  = body.fromJson(ClienteRest)
					c.validar
					var Cliente cfin = new Cliente (c,this.chino);
					cfin.carritoVirtual = new CarritoVirtual();
					cfin.saldoDisponible = 0
					this.chino.validarNombreClienteUsuario(cfin);
					this.chino.agregarCliente(cfin)
					ok('''{ "id" : "«cfin.id»" }''')
					} 
					catch (UserException e) {
					// badRequest(''' { "error" : "«e.message»" }''')
						badRequest(e.message)
					}
	}
	@Post("/comprar")
    def emitirOrden(@Body String body) {
        response.contentType = ContentType.APPLICATION_JSON
        try {
	        val ClienteRest c = body.fromJson(ClienteRest)
	        System.out.println(c.id);
	        val Cliente cl = new Cliente(c,this.chino);
	        try {
	        	 var cli = this.chino.comprar(cl);
	        	 var ClienteRest c1 =  new ClienteRest(cli);
				 ok(c1.toJson);        	
	           } 
	        catch (UserException exception) {
	        	badRequest(getErrorJson(exception.message))
	        	   }
           } 
        catch (UnrecognizedPropertyException exception) {
        	badRequest(getErrorJson("El body debe ser un OrdenRequest"))
        }
    }	
	@Post("/Users")
	def createEmployee(@Body String body){
		response.contentType = ContentType.APPLICATION_JSON
		try{
			val Empleado u  = body.fromJson(Empleado)
			try{
				this.chino.agregarEmpleado(u)
				ok()
			}
			catch(UserException e){
				badRequest(getErrorJson(e.message))
			}
		}
		catch (UnrecognizedPropertyException exception) {
			badRequest(getErrorJson("El body debe ser un Villano"))
		}
	}
	private def getErrorJson(String message) {
        '{ "error": "' + message + '" }'
	}		
//	
}