package APIRest

import appChino.Producto
import appChino.App
import org.uqbar.xtrest.api.XTRest
import appChino.Cliente
import appChino.CarritoVirtual

class ChinoVirtualApp {
	
	static Producto p;
	static Producto p2;
	static Producto p3;
	static Producto p4;
	static Producto p5;
	static Producto p6;
	static Producto p7;
	static Producto p8;
	static Producto p9;
	static Producto p10;
	static Cliente c1;
	static Cliente c2;
	static Cliente c3;
	
	static App chino;
	def static void main(String[] args) {
		p = new Producto("Bebible", "Manaos", " Si tomas demasiado te convertiras en una tortuga ninja", 20, 1, 1);
		p.setImagen("images/Manaos.jpg");
		p2 = new Producto("Comestible", "Arroz Gallo  Oro", "bajo en calorias, arroz integral sin agregados ni conservantes, Colesterol:0g", 32, 12, 2);
		p2.setImagen("images/GalloOro.jpg");
		
		p3 = new Producto("Comestible", "Dulce de leche La Serenisima", "bajo en calorias, Producto lacteo , Colesterol:0g, Grasas Trans: 12g", 55, 8, 3);
		p3.setImagen("images/dulceDeLeche.jpg");
		
		p4 = new Producto("Comestible", "Queso Crema  Finlandia La Serenisima", "bajo en calorias, Producto lacteo light , Colesterol:0g, Queso light para los gorditos ", 45, 7, 4);
		p4.setImagen("images/quesoFinlandia.jpg");
		
		p5 = new Producto("Comestible", "Fideos Luchetti", "fideos mostacholle, Producto hecho con trigo pampeano ", 95, 2, 5);
		p5.setImagen("images/fideosLuchetti.jpg");
		
		p6 = new Producto("Bebible", "Fernet Branca", "El elixir de la vida  ", 150, 5, 6);
		p6.setImagen("images/fernetBranca.jpg");
		
		p7 = new Producto("Tonico revitalizante", "simpson e hijo", "Hará bailar tus calsones", 50, 10, 7);
		p7.setImagen("images/tonico.jpg");
		
		p8 = new Producto("Comestible", "Presto pronta", "altas polentas", 25, 20, 8);
		p8.setImagen("images/polenta.jpg");
		
		p9 = new Producto("Bebible", "Brahma", "ideal para tomar con amigos", 23, 15, 9);
		p9.setImagen("images/brahma.jpg");
		
		p10 = new Producto("Bebible", "Leche La Serenisima", "leche entera, fuente de vitaminas", 24, 20, 10);
		p10.setImagen("images/leche.jpg"); 
		
		chino = new App();
		c1 = new Cliente("Juancho899","Juancho", "Talarga","25","Junacho67@gmail.com","Av.calchaquie 1065",chino);
		c1.password = "lalala";
		c2 = new Cliente("Gago05","Fernando", "Gago","32","Gagito-kpo87@gmail.com","Av.calchaquie 1055",chino);
		c2.password = "das";
		c3 = new Cliente("CarlitaTKM","Carla", "Succo","19","Carlita@gmail.com","Av.calchaquie 1075",chino);
		c3.password = "calabreza15";
		c1.carritoVirtual = new CarritoVirtual();
		c1.saldoDisponible = 100;
		c2.carritoVirtual = new CarritoVirtual();
		c2.saldoDisponible = 100;
		c3.carritoVirtual = new CarritoVirtual();
		c3.saldoDisponible = 100;
		chino.agregarCliente(c1);
		chino.agregarCliente(c2);
		chino.agregarCliente(c3);
		chino.agregarProducto(p);
		chino.agregarProducto(p2);
		chino.agregarProducto(p3);
		chino.agregarProducto(p4);
		chino.agregarProducto(p5);
		chino.agregarProducto(p6);
		chino.agregarProducto(p7);
		chino.agregarProducto(p8);
		chino.agregarProducto(p9);
		chino.agregarProducto(p10);
		var chinovirtual = new ChinoVirtualAPIRest(chino)
		XTRest.startInstance(chinovirtual, 9032)
	}
}