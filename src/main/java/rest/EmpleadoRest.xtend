package rest

import appChino.Empleado
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class EmpleadoRest extends UsuarioRest{

	String cuil
	
	new(){}
	new(Empleado em){
		super(em)
		this.cuil = em.cuil
	}
}