package rest

import appChino.Cliente
import appChino.CarritoVirtual
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException

@Accessors
class ClienteRest extends UsuarioRest{
	CarritoVirtual carrito
	private String edad;
	private String domicilio;
	private int saldoDisponible;
	
	new(){}
	new(Cliente cliente) {
		super(cliente);
		this.carrito = cliente.carritoVirtual
		this.edad = cliente.edad
		this.domicilio = cliente.domicilio
		this.saldoDisponible = cliente.saldoDisponible
	}
	
	def void validar() {
		if (this.nombreUsuario == null) {
			throw new UserException("Debe ingresar un Nombre De Usuario")
		}
		
		if (this.nombre == null) {
			throw new UserException("Debe ingresar un Nombre ")
		}
		if (this.apellido == null) {
			throw new UserException("Debe ingresar un Apellido")
		}
		if (this.email == null) {
			throw new UserException("Debe ingresar un EMail")
		}
	}
	
}