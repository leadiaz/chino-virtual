package rest

import java.util.List
import appChino.Usuario
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class UsuarioRest {
	protected String nombreUsuario;
	protected String nombre;
	protected String apellido;
	protected String email;
	protected String password 
	//protected AppRest app;
	protected int id;
	
	new (){}
	new(Usuario usuario) {
		this.nombreUsuario = usuario.nombreUsuario
		this.nombre = usuario.nombre
		this.apellido = usuario.apellido
		this.email = usuario.email
		this.password = usuario.password
		this.id = usuario.id
		//this.app  = new AppRest(usuario.app)
	}
	
}