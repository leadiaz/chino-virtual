package rest

import appChino.App
import java.util.List
import java.util.ArrayList
import appChino.*
import org.eclipse.xtend.lib.annotations.Accessors
import appChino.Cliente

@Accessors
class AppRest {

	//private List<UsuarioRest> usuarios;
	private List<ClienteRest> clientes = new ArrayList<ClienteRest>();
	private List<EmpleadoRest> empleados = new ArrayList<EmpleadoRest>();
	private List<Producto> productos = new ArrayList<Producto>(); 
	private List<Comentario> comentarios =  new ArrayList<Comentario>();
	private int recaudacion;
	
	
	new (App chino){
		this.clientes = chino.clientes.map[new ClienteRest(it)]
		this.empleados = chino.empleados.map[new EmpleadoRest(it)]
	//	this.usuarios = chino.usuarios.map[new UsuarioRest(it) ]
		this.productos  = chino.productos
		this.comentarios = chino.comentarios
		this.recaudacion = chino.recaudacion
	}
	
	def ClienteRest buscarCliente(String nombreUser) {
		var cliente = clientes.findFirst [ 
			it.nombreUsuario == nombreUser
			]
			if(cliente == null){
				throw new RuntimeException("No tengo ese Usuario, man. (el del nombreUsuario = " + nombreUser + ")")
			}
		return cliente;
	}
}