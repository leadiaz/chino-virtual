package appChino;

import org.eclipse.xtend.lib.annotations.Accessors;

@Accessors
public class Producto {
	private String imagen;
	private String tipoDeProducto;
	private String marca;
	private String descripcion;
	private int precio;
	private int stockDisponible;
	private int id;
	
	public Producto(){}
	
	public Producto(String tipo, String marca, String descripcion, int precio, int stock, int id){	
		this.setTipoDeProducto(tipo);
		this.setMarca(marca);
		this.setDescripcion(descripcion);
		this.setPrecio(precio);
		this.setStockDisponible(stock);
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public String getImagen(){
		return imagen;
	}

	public String getTipoDeProducto() {
		return tipoDeProducto;
	}
	
	public void setImagen(String url){
		this.imagen = url;
	}
	
	public void setTipoDeProducto(String tipoDeProducto) {
		this.tipoDeProducto = tipoDeProducto;
	}

	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getStockDisponible() {
		return stockDisponible;
	}

	public void setStockDisponible(int stockDisponible) {
		this.stockDisponible = stockDisponible;
	}
	

}
