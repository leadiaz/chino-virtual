package appChino;

import java.math.BigInteger;

public class Empleado extends Usuario{

	private String cuil;

	public Empleado(String nombre, String apellido,String cuil ,String email, App app){
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setCuil(cuil);
		this.setEmail(email);
		this.setApp(app);
		this.id = app.cantidadDeClientes();
	}
	
	public void agregarProducto(Producto unProducto) {
		this.getApp().agregarProducto(unProducto);		
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

}
