package appChino;

import java.util.ArrayList;

import org.eclipse.xtend.lib.annotations.Accessors;
@Accessors
public class CarritoVirtual {
	
	private ArrayList<Producto> productosAcumulados;
	private int montoAcumulado;
	
	

	public ArrayList<Producto> getProductosAcumulados() {
		return productosAcumulados;
	}


	public void setProductosAcumulados(ArrayList<Producto> productosAcumulados) {
		this.productosAcumulados = productosAcumulados;
	}
	
	public void agregarProducto(Producto producto){
		this.productosAcumulados.add(producto);
		this.montoAcumulado += producto.getPrecio();
	}



	public int getMontoAcumulado() {
		return montoAcumulado;
	}




	public void setMontoAcumulado(int montoAcumulado) {
		this.montoAcumulado = montoAcumulado;
	}
	
	
	public CarritoVirtual(){
		
		this.setProductosAcumulados(new ArrayList<Producto>());
		this.setMontoAcumulado(0);
	}
	
	
	public int cantidadDeProductosEnChanguito(){
		
		return this.getProductosAcumulados().size();
	}




	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}




}
