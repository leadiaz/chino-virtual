package appChino;

import java.math.BigInteger;

import org.eclipse.xtend.lib.annotations.Accessors;

import rest.ClienteRest;

@Accessors
public class Cliente extends Usuario{
	
	private String edad;
	private String domicilio;
	private int saldoDisponible;
	private CarritoVirtual carritoVirtual;

	public Cliente(){	
	}
	
	public Cliente(ClienteRest cl, App app){
		this.setPassword(cl.getPassword());
		this.setNombreUsuario(cl.getNombreUsuario());
		this.setNombre(cl.getNombre());
		this.setApellido(cl.getApellido());
		this.setEdad(cl.getEdad());
		this.setEmail(cl.getEmail());
		this.setDomicilio(cl.getDomicilio());
		this.setSaldoDisponible(cl.getSaldoDisponible());
		this.setCarritoVirtual(cl.getCarrito());
		this.setId(cl.getId());
	}
	public Cliente(String nombreUSer,String unNombre, String unApellido, String unaEdad, String unCorreo, String unDomicilio, App unaApp){
		this.setNombreUsuario(nombreUSer);
		this.setNombre(unNombre);
		this.setApellido(unApellido);
		this.setEdad(unaEdad);
		this.setEmail(unCorreo);
		this.setDomicilio(unDomicilio);
		this.setSaldoDisponible(0);
		this.setCarritoVirtual(null);
		//this.id = unaApp.cantidadDeClientes() + 1;// esto genera el id automaticamente dependiendo de la cant de usuarios en la app
	}
	
	public String getEdad() {
		return edad;
	}



	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getDomicilio() {
		return domicilio;
	}
	public int getId(){
		return id;
	}



	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	
	

	public int getSaldoDisponible() {
		return saldoDisponible;
	}



	public void setSaldoDisponible(int saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}



	public CarritoVirtual getCarritoVirtual() {
		return carritoVirtual;
	}



	public void setCarritoVirtual(CarritoVirtual carritoVirtual) {
		this.carritoVirtual = carritoVirtual;
	}


	
	public void registrarme(App unaApp){
		
		/*
		 * PROP: se crea un changuito virtual nuevo, se le asigna dicho carrito al cliente
		 *       una vez registrado, y por último se agrega al cliente a la lista de clientes 
		 *       registrados a la aplicación pasada como parámetro.
		 */
		CarritoVirtual changuito = new CarritoVirtual();
		this.setCarritoVirtual(changuito);
		unaApp.agregarCliente(this);
	}
	
	
    
	public void recargarSaldo(int unMonto){
		
		this.saldoDisponible = this.getSaldoDisponible() + unMonto;
	}
	
	
   public void agregarProducto(Producto unProducto) {
		
		/*
		 * PROP: si el cliente no tiene asignado un carrito virtual, es porque no está 
		 *       registrado y, por ende, no puede adquirir determinado producto. Si tiene
		 *       asignado un carrito virtual, entonces está registrado, y puede adquirir dicho
		 *       producto.   
		 */
	      
	      if(this.getCarritoVirtual() == null){
	       System.out.println("registrate!");
	      }
	      else{
	       this.getCarritoVirtual().agregarProducto(unProducto);
	      } 
	  }
   
   
   public void realizarCompra(App unaApp){
		  
	      this.saldoDisponible = this.getSaldoDisponible() - this.getCarritoVirtual().getMontoAcumulado(); 	  
          unaApp.incementarRecaudacion(this.getCarritoVirtual().getMontoAcumulado()); 
   }
   
   
   public void realizarComentario(String unComentario, int unaPuntuacion, App unaApp){
	   
	   Comentario comentarioYPuntuacion = new Comentario(unComentario, unaPuntuacion);
	   unaApp.agregarComentario(comentarioYPuntuacion);
   }
   
   
   


}
