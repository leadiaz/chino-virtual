package appChino;

public class Comentario {
	
/*
 * OBS: esta clase no necesita ser testeada, ya que su única función es instanciar
 * un comentario que el cliente hará a la aplicación.
 */
	
	private String comentario;
	private int puntuacion;
	
	
	
	public String getComentario() {
		return comentario;
	}



	public void setComentario(String comentario) {
		this.comentario = comentario;
	}



	public int getPuntuacion() {
		return puntuacion;
	}



	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}
	
	
	public Comentario(String unComentario, int unaPuntuacion) {
		
		this.setComentario(unComentario);
		this.setPuntuacion(unaPuntuacion);
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
