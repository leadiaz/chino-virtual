package appChino;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.xtend.lib.annotations.Accessors;
import org.uqbar.commons.model.UserException;
import appChino.Producto;
@Accessors
public class App {
	
	//private List<Usuario> usuarios;
	private List<Cliente> clientes;
	private ArrayList<Empleado> empleados;
	private List<Producto> productos;
	private ArrayList<Comentario> comentarios;
	private int recaudacion;
	

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void eliminarCliente(int id){
		List<Cliente> aux = new ArrayList<Cliente>();
		for (Cliente p: clientes) {
			if (p.getId() != id) {
				aux.add(p);
			}
		}
		this.clientes = (ArrayList<Cliente>) aux;
	}
	public void actualizarCliente(Cliente clienteActualizado){
		this.eliminarCliente(clienteActualizado.id);
		this.clientes.add(clienteActualizado);
		
	}

	public void setClientes(ArrayList<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	public Cliente getCliente(int idcl){
		Cliente aux = null; 
		for (Cliente p: clientes) {
			if (p.id == idcl){
				aux = p;
			}
		}
		return aux;
	}
	
	public Cliente comprar(Cliente c){
		
		if(c.getSaldoDisponible() < c.getCarritoVirtual().getMontoAcumulado()){
			throw new UserException("No hay Dinero suficiente en tu billetera");
		}else{
		c.setSaldoDisponible( c.getSaldoDisponible()- c.getCarritoVirtual().getMontoAcumulado());
		for (Producto p: c.getCarritoVirtual().getProductosAcumulados()) {
	
			if(p.getStockDisponible() == 0){
				throw new UserException("No hay Stock");
			}else{
			 p.setStockDisponible(p.getStockDisponible()-1);
			 this.actualizarProducto(p);
				}
		}
		CarritoVirtual carrito =new CarritoVirtual();
		c.setCarritoVirtual(carrito);
		this.actualizarCliente(c);
		return c;
		}}
	
	
	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}



	public void setEmpleados(ArrayList<Empleado> empleados) {
		this.empleados = empleados;
	}
	
	
	
	
	public List<Producto> getProductos() {
		return productos;
	}



	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}
	
	
	
	public int getRecaudacion() {
		return recaudacion;
	}


	public void setRecaudacion(int recaudacion) {
		this.recaudacion = recaudacion;
	}
	
	
	public ArrayList<Comentario> getComentarios() {
		return comentarios;
	}


	public void setComentarios(ArrayList<Comentario> comentarios) {
		this.comentarios = comentarios;
	}



	public App(){
		this.setClientes(new ArrayList<Cliente>());
		this.setEmpleados(new ArrayList<Empleado>());
		this.setProductos(new ArrayList<Producto>());
		this.setComentarios(new ArrayList<Comentario>());
		//this.usuarios = new ArrayList<Usuario>();
		this.setRecaudacion(0);
	}
	
	/*public List<Usuario> getUsuarios(){
		return this.usuarios;
	}
	public void addUser(Usuario user) {
		this.usuarios.add(user);
	}*/
	
	public void validarNombreClienteUsuario(Cliente unCliente){
		for(Cliente cl : this.clientes){
			if(cl.nombreUsuario == unCliente.nombreUsuario){
				throw new UserException("Ese Nombre De Usuario Ya Existe");
			}
		}
	}
	public void agregarCliente(Cliente unCliente){
		
		//this.validarNombreClienteUsuario(unCliente);
		unCliente.setId(this.getClientes().size());
		this.getClientes().add(unCliente);
		//this.addUser(unCliente);
	}
	
	
	public void agregarEmpleado(Empleado unEmpleado){
		unEmpleado.setId(this.getEmpleados().size());
		this.getEmpleados().add(unEmpleado);
		//this.addUser(unEmpleado);
	}
	
	
	public int cantidadDeClientes(){
		
		return this.getClientes().size();
	}
	
	public int cantidadDeEmpleados(){
		
		return this.getEmpleados().size();
	}
	
	
	public int cantidadDeProductos() {
		
		return this.getProductos().size();
	}
	
	
	public void agregarProducto(Producto unProducto) {
		this.getProductos().add(unProducto);
		
	}
	
	public boolean disponibilidadDeUnProducto(Producto producto){
		// Producto disponible o no en la lista de productos de la aplicación.
		return this.getProductos().contains(producto);
	}
	
	public void incementarRecaudacion(int monto){
		this.recaudacion = this.getRecaudacion() + monto;
	}
	
	
	public void agregarComentario(Comentario unComentario){
		
       this.getComentarios().add(unComentario);		
	}
	
	
	public int cantidadDeComentarios(){
		
		return this.getComentarios().size();
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

	}

	public void eliminarProducto(int id) {
		List<Producto> aux = new ArrayList<Producto>();
		for (Producto p: productos) {
			if (p.getId() != id) {
				aux.add(p);
			}
		}
		this.productos = (ArrayList<Producto>) aux;
	}



	public void actualizarProducto(Producto producto) {
		this.eliminarProducto(producto.getId());
		productos.add(producto);
	}



	public void imprimirProductos() {
		for(Producto p : productos) {
			System.out.print("[");
			System.out.print("Tipo: "+ p.getTipoDeProducto());
			System.out.print(" ,Marca: "+ p.getMarca());		
			System.out.print(" ,id: "+p.getId());		
			System.out.print(" ,stock: "+ p.getStockDisponible());
			System.out.println("]");
			
		}
		
	}

//	public List<Producto> searchProducto(String string) {
//		if(string == "") {
//			return productos;
//		} else {
//			List<Producto> aux = new ArrayList<Producto>();
//			for(Producto p : this.productos) {
//				if(p.)
//				
//			}
//			return this.productos.//[ it.getNombrePais.toLowerCase.contains(substring.toLowerCase) ].toList			
//	}
//	}


}
