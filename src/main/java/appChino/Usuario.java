package appChino;

import java.math.BigInteger;

public abstract class Usuario {
	protected String nombreUsuario;
	protected String nombre;
	protected String apellido;
	protected String email;
	//faltaria el password
	protected String password;
	protected App app;
	protected int id;
	
	public  int getId() {
		return id;
	}
	public String getNombreUsuario(){
		return nombreUsuario;
	}
	
	public void setNombreUsuario( String n){
		this.nombreUsuario = n;
	}
	public void setId(int id){
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public String getPassword(){
		return this.password;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public App getApp() {
		return app;
	}
	
	public void setPassword(String p){
		this.password = p;
	}
	public void setApp(App app) {
		this.app = app;
	} 
	
}
