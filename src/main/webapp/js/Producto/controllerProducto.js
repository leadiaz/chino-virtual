app.controller('TodosLosProductosCtrl', function ($resource, $timeout, Productos,Clientes ,ClientesBuscar ,Comprar,ClientesAgregar) {
	'use strict';
	
	console.log("Inicializando");
	var self = this;
	self.productos = [];
	this.productoSeleccionado = null;
	
	this.actualizarLista = function() {
		Productos.query(function(data) {
            self.productos = data;
        });
    };
    
  this.actualizarLista();
	
	
	 this.verDescripcion = function(producto) {
	        self.productoSeleccionado = producto;
	        console.log(self.productoSeleccionado.descripcion);
	        $("#verDescripcionProductoModal").modal({});
	    };    
	//////////////////
	    
	    console.log("InicializandoControllerUsuario");
		
		//var self = this;
		self.nombreUser = '';
		self.contrasenha = '';
		self.clientes = [];
		self.saldoACargar= 0;
		//var nuevoCliente
		self.usuarioIniciado = null;
		self.usuario = null;
		
		this.actualizarListaC = function() {
			Clientes.query(function(data) {
	            self.clientes = data;
	        });
	    };
	    
	    this.actualizarListaC();
		 // AGREGAR
	    this.agregarCliente = function() {
	    	ClientesAgregar.save(this.nuevoCliente, function(data) {
	    		//console.log(this.nuevoCliente.nombre);
	    		 //self.notificarMensaje('Gracias Por Registrarse');
	            self.actualizarListaC();
	          //  console.log("usuarioSeAgrego" + data.nombreUsuario);
	            self.nuevoCliente = null;
	        });
	        $("#registroModal").modal('toggle');
	    };
	    
	   //Iniciar Session
	    this.aceptar = function(){
	    	if(self.nombreUser == ''|| self.contrasenha == '' ){
	    		alert("Completa los campos de Inicio de Sesion")
	    	}else{
	    		ClientesBuscar.queryCliente({"nombreUsuario":self.nombreUser},function(data){
	    		console.log("UsaurioNombreData:" + data.nombreUsuario +"Pass:"+data.password );
	    		if(self.contrasenha != data.password){alert("Password Incorrecta")}else{
	    		self.usuarioIniciado = data;
	    		console.log("UsuarioNombreSelf:" + self.usuarioIniciado.nombreUsuario +"Pass:"+self.usuarioIniciado.password );
	    		self.nombreUser = '';
	    		self.contrasenha = '';	
	    		$("#myModalInicioDeSesion").modal('toggle')
	    		
	    		}
	    	});	
	    	console.log("UsaurioNombre:" +self.nombreUser +"Pass:"+self.contrasenha );
	    	//$("#myModalInicioDeSesion").modal('toggle');
	    	}
		};
		
	    this.seguirComprando = function(){
	    	//console.log("UsuarioNombreSelf:" + self.usuarioIniciado.nombreUsuario +"Pass:"+ self.usuarioIniciado.password );
	    	$("#myModalCarrito").modal('toggle');
	    };
	    
	    this.comprar = function(){
	    	//console.log("UsuarioNombreSelf:" + self.usuarioIniciado.nombreUsuario +"Pass:"+ self.usuarioIniciado.password );
	    	if(this.usuarioIniciado.saldoDisponible < this.usuarioIniciado.carrito.montoAcumulado ){
	    		alert("Saldo Insuficiente");
	    	}else{
	    	Comprar.buy(this.usuarioIniciado,function(data) {
	           	self.usuarioIniciado = data;
	           	self.actualizarLista()
	           	console.log("UsuarioNombreSaldo"+ data.saldoDisponible );
	        });
	    	alert("Gracias por Comprar, Vuelva pronto");
	    	$("#myModalCarrito").modal('toggle');
	    	}
	    };
		
	    this.verDescripcionC = function(cliente) {
	        this.usuario = cliente;
	        console.log(self.usuario.nombreUsuario);
	        $("#verDescripcionClienteModal").modal({});
	    };
	    
	    this.seleccionarProductoYAgregarloAlCarrito = function(producto) {
	  	  if(self.usuarioIniciado == null){
	  		alert("Debes Iniciar Sesion");
	  	  }else{
	  		  	if(producto.stockDisponible == 0){
	  		  		alert("No hay Stock Disponible De Este Producto")
	  		  	}else{
	  		  	
	  		  	console.log("Seleccionando " + producto);
	  		  	this.productoSeleccionado = producto;
	  		  	console.log("Seleccionando " + this.productoSeleccionado.marca );
	  		  	this.usuarioIniciado.carrito.productosAcumulados.push(producto);
	  		  	console.log("Seleccionando " + this.usuarioIniciado.carrito.montoAcumulado );
	  		  	this.usuarioIniciado.carrito.montoAcumulado  = this.usuarioIniciado.carrito.montoAcumulado  + producto.precio;
			//this.guardarCliente();
	  		  	}
	  		
	  	  }
	  	};
	  	
	  	this.guardarCliente  = function() {
	  		Clientes.updates(this.usuarioIniciado,function(){
	  			console.log("UsuarioAActualizar " + self.usuarioIniciado.nombreUsuario );
	            self.actualizarListaC();
	        });
	    };
	    
	    this.borrarProductoDelCarrito = function(producto){
	    	this.usuarioIniciado.carrito.montoAcumulado  = this.usuarioIniciado.carrito.montoAcumulado  - producto.precio;
	    	console.log("Seleccionando para borrar " + producto.marca );
			this.usuarioIniciado.carrito.productosAcumulados =this.usuarioIniciado.carrito.productosAcumulados.filter(
				function(it) {
					return it.marca!=producto.marca;
				})
	    }
	    
	    this.verCarritoModal = function() {
	    	if(self.usuarioIniciado == null){
	    		alert("Debes iniciar Sesion")
	    	}else{
	        console.log(self.usuarioIniciado.nombreUsuario);
	        $("#myModalCarrito").modal({});
	    	}
	    };
	    
	    this.cargarSaldo = function() {
	    	if(self.saldoACargar == 0){
	    		alert("Debes colocar una cantidad para agregar")
	    	}else{
	        console.log(self.usuarioIniciado.saldoDisponible);
	        console.log(self.saldoACargar);
	        this.usuarioIniciado.saldoDisponible = this.usuarioIniciado.saldoDisponible + self.saldoACargar;
	        self.saldoACargar = 0;
	    	}
	    };
	    
	    this.cerrarSesion = function() {
	    	if(self.usuarioIniciado == null){
	    		alert("Debes iniciar Sesion")
	    	}else{
	        console.log(self.usuarioIniciado.nombreUsuario);
	        self.usuarioIniciado = null;
	        $("#myModalCarrito").modal('toggle');
	    	}
	    };
	
});