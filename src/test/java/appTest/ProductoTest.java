package appTest;

import org.junit.Before;
import org.junit.Test;

import appChino.*;
import junit.framework.TestCase;


public class ProductoTest extends TestCase{
	
	private Producto producto;

	@Before
	public void setUp() throws Exception {
		
		producto = new Producto ("fideos", "Matarazzo", "fideos para tallarines del domingo", 15, 100, 4);
	}

	@Test
	public void testTipoDeProducto() {
		assertTrue(producto.getTipoDeProducto() == "fideos");
	}
	
	@Test
	public void testStockDisponible() {
		assertTrue(producto.getStockDisponible() == 100);
	}

}
