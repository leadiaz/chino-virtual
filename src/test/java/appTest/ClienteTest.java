package appTest;

import org.junit.Before;
import org.junit.Test;

import appChino.*;
import junit.framework.TestCase;

public class ClienteTest extends TestCase {
	
	private Cliente cliente;
	private App app; 
	private Producto producto1;
	

	@Before
	public void setUp() throws Exception {
		app = new App ();
		cliente = new Cliente("EnsiasElWachin","ensias", "sangrantes", "45", "reydelblues@gmail.com", "callefalsa123",app);
		producto1 = new Producto("aceitunas", "Yaoming", "frasco de aceitunas para alta pizza", 25, 40, 0);	// revisar esto por favor 	
	}
	

	@Test
	public void testNombre() {
		assertTrue(cliente.getNombre() == "ensias");
	}
	
	@Test
	public void testApellido() {
		assertTrue(cliente.getApellido() == "sangrantes");
	}
	
	@Test
	public void testEdad() {
		assertTrue(cliente.getEdad() == "45");
	}
	
	@Test
	public void testRegistrarme() {
		cliente.registrarme(app);
		assertTrue(app.cantidadDeClientes() == 1);
		assertTrue(cliente.getCarritoVirtual().getMontoAcumulado() == 0);
	}
	
	@Test
	public void testDatosChanguito(){
		cliente.registrarme(app);
		assertTrue(cliente.getCarritoVirtual().cantidadDeProductosEnChanguito() == 0);
        assertTrue(cliente.getCarritoVirtual().getMontoAcumulado() == 0);	
	}
	
	@Test
	public void testAgregarProducto(){
		cliente.registrarme(app);
		cliente.agregarProducto(producto1);
		assertTrue(cliente.getCarritoVirtual().cantidadDeProductosEnChanguito() == 1);
	}
	
	@Test
	public void testVerificarMontoAcumulado(){
		assertTrue(cliente.getCarritoVirtual() == null);
		cliente.registrarme(app);
		assertTrue(cliente.getCarritoVirtual().getMontoAcumulado() == 0);
		cliente.agregarProducto(producto1);
		assertTrue(cliente.getCarritoVirtual().getMontoAcumulado() == 25);
	}
	
	@Test
	public void testRecargarSaldo(){
		assertTrue(cliente.getSaldoDisponible() == 0);
		cliente.recargarSaldo(500);
		assertTrue(cliente.getSaldoDisponible() == 500);
	}
	
	@Test
	public void testRealizarCompraEincrementarRecaudacion(){
		cliente.registrarme(app);
		cliente.recargarSaldo(500);
		cliente.agregarProducto(producto1);
		assertTrue(cliente.getSaldoDisponible() == 500);
		cliente.realizarCompra(app);
		assertTrue(cliente.getSaldoDisponible() == 475);
		assertTrue(app.getRecaudacion() == 25);
	}
	
	 
	@Test
	public void testRealizarComentario(){
		assertTrue(app.cantidadDeComentarios() == 0);
		cliente.realizarComentario("excelente servicio mono!", 2, app);
		assertTrue(app.cantidadDeComentarios() == 1);
	}
}
