package appTest;

import java.math.BigInteger;

import org.junit.Test;

import appChino.*;
import junit.framework.TestCase;

public class AppTest extends TestCase {
	
	private Usuario cliente1;
	private Usuario empleado1;
	private Producto producto1;
	private App app;
	
	
	public void setUp(){
		
		app = new App();
		empleado1 = new Empleado("Cosme", "Fulanito", "20305553337", "quieneshomero@gmail.com", app);
		cliente1 = new Cliente("Tittip","Tito", "Puente", "32", "titopuente@gmail.com", "callefalsa123", app);
		producto1 = new Producto("harina", "Favorita", "para la torta de cumple", 20, 40, 9);
		
	}

	@Test
	public void testCantidadClientes() {
		
		assertTrue(app.cantidadDeClientes() == 0);
		app.agregarCliente((Cliente) cliente1);
		assertTrue(app.cantidadDeClientes() == 1);
	}

	@Test
	public void testCantidadEmpleados(){
		assertTrue(app.cantidadDeEmpleados() == 0);
		app.agregarEmpleado((Empleado) empleado1);
		assertTrue(app.cantidadDeEmpleados() == 1);
	}
	
	@Test
	public void testEmpleadoAgregaProductoALaApp(){
		assertTrue(app.cantidadDeProductos() == 0);
		((Empleado) empleado1).agregarProducto(producto1);
		assertTrue(app.cantidadDeProductos() == 1);
	}
	
	@Test
	public void testDisponibilidadDeUnProducto(){
		assertTrue(app.disponibilidadDeUnProducto(producto1) == false);
		app.agregarProducto(producto1);
		assertTrue(app.disponibilidadDeUnProducto(producto1) == true);
	}
	
	@Test
	public void testIncrementarRecaudacion(){
		assertTrue(app.getRecaudacion() == 0);
		app.incementarRecaudacion(10);
		assertTrue(app.getRecaudacion() == 10);
	}
	
	@Test
	public void testActualizarProducto()throws Exception{
		app.agregarProducto(producto1);
		app.imprimirProductos();
		Producto ptest = new Producto("harina", "Favorita", "para la torta de cumple", 20, 39, 9);

		app.actualizarProducto(ptest);
		assertEquals(app.getProductos().size(), 1);
		app.imprimirProductos();
	}
	
	
	
}
