package appTest;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import appChino.*;
import junit.framework.TestCase;

public class EmpleadoTest extends TestCase {
	
	private Empleado empleado;
	private App app;
	private Producto producto;

	@Before
	public void setUp() throws Exception {
		
		app = new App();
		empleado = new Empleado("juanito", "arcoiris","20-35100333-8", "juanito@gmail.com", app);
		producto = new Producto ("aceite", "Marolio", "marolio le da sabor a tu vida", 18, 80, 5);
	}

	@Test
	public void testApellido() {
		assertTrue(empleado.getApellido() == "arcoiris");
	}
	
	@Test	
	public void testCuil() {
		assertTrue( ((Empleado) empleado).getCuil() == "20-35100333-8");
	}
	
	@Test
	public void testAgregarProducto() {
		empleado.agregarProducto(producto);
        assertTrue(app.cantidadDeProductos() == 1);	
	}

}
